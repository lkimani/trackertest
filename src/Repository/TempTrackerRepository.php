<?php

namespace App\Repository;

use App\Entity\TempTracker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TempTracker|null find($id, $lockMode = null, $lockVersion = null)
 * @method TempTracker|null findOneBy(array $criteria, array $orderBy = null)
 * @method TempTracker[]    findAll()
 * @method TempTracker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TempTrackerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TempTracker::class);
    }

    // /**
    //  * @return TempTracker[] Returns an array of TempTracker objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TempTracker
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
