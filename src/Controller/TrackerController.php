<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TempTracker;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Bundle\FrameworkBundle\Controller\Redis;

class TrackerController extends AbstractController
{
    /**
     * Initialize cache adapter constructor
     *
     * @param AdapterInterface $cache
     */
    public function __construct(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }
   
   /**
    * Utility function to manage cache operations
    *
    * @param [type] $key
    * @param [type] $value
    * @param boolean $max
    * @return void
    */
    private function manageCache($key, $value , $max=false)
    {
        // $this->cache->clear();
        $item = $this->cache->getItem($key);
        if (!$item->isHit()) {
            $item->set($value);
            $this->cache->save($item);
        }
        if ($this->cache->hasItem($key)) {
            $oldValue = $this->cache->getItem($key)->get();
            
            if ($max==true) {
                $item->set($value >= $oldValue ? $value:$oldValue);
            } else {
                $item->set($value >=  $oldValue ? $oldValue:$value);
            }
            
            $this->cache->save($item);
            $this->cache->getItem($key);
        }
        return $this->cache->getItem($key)->get();
    }
    /**
     * @Route("/tracker", name="new_temp")
     */
    public function saveNewTemp(): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();
        $data = json_decode($request->getContent(), true);
        $tempTracker = new TempTracker();
        $tempTracker->setDateCreated(new \DateTime('now'));
        $tempTracker->setTemparature($data['temp']);
        $tempTracker->setTrackerID(\rand(0, 100) .\rand(100, 999)) ;
        $entityManager->persist($tempTracker);
        $entityManager->flush();

        //update cache: MAX;
       $this->manageCache("max",$tempTracker->getTemparature(),true); 
        //update cache: MIN;
        $this->manageCache("min",$tempTracker->getTemparature(),false);

        return new Response($this->json([
                    'message' =>  'Saved new tempTracker with id '.$tempTracker->getId(),
                    'temp' => $tempTracker->getTemparature(),
                ]));
    }

    /**
     * Get max temparature from cache on fail fall back to database.
     * @Route("/max", name="new_temp")
     *
     * @return Response
     * */
    public function getMax(): response
    {
        $maxTemps =0;
        //reteieve from cache, if fails check DB;

        $maxTemps=$this->manageCache("max", "0", true);

        if ($maxTemps>0) {
            return new Response($this->json([
                'message' =>  'Maximum tracked Temparature ' ,
               'temp' => $maxTemps,
           ]));
        }

        /**
         * This block will only be executed if value not found on cache.
         *  Results in querying database
         */
        $trackerRepo = $this->getDoctrine()
            ->getRepository(TempTracker::class);

        $query = $trackerRepo->createQueryBuilder('tt')
            ->where('tt.temparature > :minTemp')
            ->setParameter('minTemp', '1')
            ->orderBy('tt.temparature', 'DESC')
            ->getQuery();
 
        $temp =$query->setMaxResults(1)->getOneOrNullResult();

        if (empty($temp)) {
            return new Response($this->json([
                'message' =>  'No available temparature. '
            ]));
        }

        return new Response($this->json([
             'message' =>  'Max Temp with id  ' ,
            'temp' => $temp->getTemparature()
        ]));
    }

    /**
     * Get min temparature from cache on fail fall back to database.
     * @Route("/min", name="new_temp")
     *
     * @return Response
     * */
    public function getMin(): response
    {
        $minTemps =0;
        //reteieve from cache, if fails check DB;
        $minTemps=$this->manageCache("min", "0" , false);
        if ($minTemps ) {
            return new Response($this->json([
                'message' =>  'Manimum tracked Temparature ' ,
               'temp' => $minTemps,
           ]));
        }
        $trackerRepo = $this->getDoctrine()
        ->getRepository(TempTracker::class);

        $query = $trackerRepo->createQueryBuilder('tt')
        ->where('tt.temparature < :minTemp')
        ->setParameter('minTemp', '50')
        ->orderBy('tt.temparature', 'ASC')
        ->getQuery();

        $temp =$query->setMaxResults(1)->getOneOrNullResult();

        if (empty($temp)) {
            return new Response($this->json([
            'message' =>  'No available temparature. '
        ]));
        }

        return new Response($this->json([
         'message' =>  'Min Temp with id  ' ,
        'temp' => $temp->getTemparature()
    ]));
    }

    /**
     * Get average temparature from cache on fail fall back to database.
     * @Route("/average", name="new_temp")
     *
     * @return Response
     * */
    public function getAverage(): response
    {
        $trackerRepo = $this->getDoctrine()
        ->getRepository(TempTracker::class);

        $query = $trackerRepo->createQueryBuilder('tt')
        ->select('AVG(tt.temparature) ')
        ->getQuery();

        $temp =$query->setMaxResults(1)->getOneOrNullResult();

        if (empty($temp)) {
            return new Response($this->json([
            'message' =>  'No available temparature. '
        ]));
        }

        return new Response($this->json([
         'message' =>  'Max Temp with id  ' ,
        'temp' => $temp
    ]));
    }
}
