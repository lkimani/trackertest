<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\Tracker as BaseUser; 

/**
  *  
 * @ORM\Table("tracker")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class Tracker 
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
     protected $id;

     public function getId(): ?int
     {
        return $this->id;
     }
      /** 
       * 
       * @ORM\Column(type="integer", name="temparature", nullable=false, options={"unsigned":true, "default":0})
       */
    protected $temparature;
 
    /**
     * Get the value of temparature
     */ 
    public function getTemparature()
    {
        return $this->temparature;
    }

    /**  
       * @ORM\Column(type="datetime", name="dateCreated", nullable=false, options={ "default":"CURRENT_TIMESTAMP"})
       */
    private $dateCreated;
 
 

    /**
     * Set the value of temparature
     *
     * @return  self
     */ 
    public function setTemparature($temparature)
    {
        $this->temparature = $temparature;

        return $this;
    }

    
    /**
     * Get the value of dateCreated
     */ 
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set the value of dateCreated
     *
     * @return  self
     */ 
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }
 
}
?>