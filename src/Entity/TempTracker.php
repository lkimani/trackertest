<?php

namespace App\Entity;

use App\Repository\TempTrackerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 *  
 * @ORM\Entity(repositoryClass=TempTrackerRepository::class)
 */
class TempTracker
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $temparature;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="integer")
     */
    private $trackerID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTemparature(): ?int
    {
        return $this->temparature;
    }

    public function setTemparature(int $temparature): self
    {
        $this->temparature = $temparature;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getTrackerID(): ?int
    {
        return $this->trackerID;
    }

    public function setTrackerID(int $trackerID): self
    {
        $this->trackerID = $trackerID;

        return $this;
    }
}
